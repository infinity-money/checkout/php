<?php namespace Infinity\Checkout;

/**
 * Infinity Checkout PHP Library
 * Author: Irman Ahmad
 * Date: 2018-08-27
 * Time: 2:06 PM
 *
 *
 *
 */

class Response
{
    public $merchantKey;
    public $orderReference;
    public $transactionId;
    public $status;
    public $signature;

    static $VALUE_PAIRS = array(
        'merchant-key' => 'merchantKey',
        'order-reference' => 'orderReference',
        'transaction-id' => 'transactionId',
        'status' => 'status',
        'signature' => 'signature'
    );

    static function parse($data = array()){
        $response = new self();

        foreach($data as $key => $datum){
            if(in_array($key, array_keys(self::$VALUE_PAIRS))){
                $response->{self::$VALUE_PAIRS[$key]} = $datum;
            }
        }

        return $response;
    }

    function isSignatureValid($secret){
        $data = array(
            $this->merchantKey,
            $this->orderReference,
            $this->status
        );
        try {
            return ($this->signature === self::generateSignature($data, $secret));
        }catch(\Exception $e){
            return false;
        }
    }

    static function generateSignature($data, $secret){
        return base64_encode(hash_hmac('sha256', implode('~', $data), $secret, true));
    }
}