<?php namespace Infinity\Checkout;

use Infinity\Infinity;
use Infinity\Support\Arr;
use Infinity\Support\Base;

/**
 * Infinity Checkout PHP Library
 *
 * @author Irman Ahmad
 * @property Merchant $merchant
 * @property Order $order
 * @property User $user
 * @property Url $url
 *
 */

class Checkout
{
    private $js = [
        'sandbox' => 'https://checkout.sandbox.infinity.money/js/checkout.js',
        'production' => 'https://checkout.infinity.money/js/checkout.js'
    ];

    private $merchant;
    private $order;
    private $user;
    private $url;

    public $customJs = null;
    private $sandbox = true;
    private $signature;
    public $loadingText = 'Loading Infinity Payment...';
    public $buttonLabel = 'Pay with Infinity';

    function __construct($data = [])
    {
        if(!empty($data)){
            $this->fill($data);
        }
    }

    function script(){
        $jsUrl = ($this->sandbox)? $this->js['sandbox'] : $this->js['production'];
        $jsUrl = ($this->customJs)? $this->customJs : $jsUrl;

        return '<script src="'.$jsUrl.'"></script>';
    }

    /**
     * @return string
     * @throws \Exception
     */
    function button(){
        $this->validate();
        $str = '';

        $data = [
            'merchant-key' => Infinity::$publicKey,
            'button-label' => $this->buttonLabel,

            'user-email' => $this->user->email,

            'order-reference' => $this->order->reference,
            'order-description' => $this->order->description,
            'order-currency' => $this->order->currency,
            'order-total' => number_format($this->order->total, 2, '.', ''),

            'signature' => $this->signature,
        ];

        if($this->merchant && $this->merchant->photoUrl){
            $data['merchant-photo-url'] = $this->merchant->photoUrl;
        }
        if($this->url){
            if($this->url->return){
                $data['url-return'] = $this->url->return;
            }
            if($this->url->notification){
                $data['url-notification'] = $this->url->notification;
            }
        }

        if($this->loadingText){
            $str .= '<p class="infinity-preloader">'. $this->loadingText .'</p>'.PHP_EOL;
        }

        $str .= '<infinity-checkout';

        foreach($data as $key => $value){
            $str .= PHP_EOL.$key.'="'.$value.'"';
        }

        $str .= PHP_EOL.'></infinity-checkout>';

        return $str;
    }

    /**
     * @throws \Exception
     */
    function validate(){
        if(!Base::RIS(Infinity::$publicKey)){
            throw new \Exception('Infinity Error: `Public Key` is not set or invalid.');
        }
        if(!Base::RIS(Infinity::$secretKey)){
            throw new \Exception('Infinity Error: `Secret Key` is not set or invalid.');
        }
        if(!Base::RIS($this->order->reference)){
            throw new \Exception('Infinity Error: `order.reference` is not set or invalid.');
        }
        if(!Base::RIS($this->order->currency)){
            throw new \Exception('Infinity Error: `order.currency` is not set or invalid.');
        }
        if(!Base::RIS($this->order->total) || !is_numeric($this->order->total)){
            throw new \Exception('Infinity Error: `order.total` is not set or invalid.');
        }
        $this->generateSignature();
    }

    function generateSignature(){
        $data = [
            Infinity::$publicKey,
            $this->order->reference,
            $this->order->currency,
            number_format($this->order->total, 2, '.', ''),
        ];
        $this->signature = base64_encode(hash_hmac('sha256', implode('~', $data), Infinity::$secretKey, true));
    }

    function fill($data){
        if(isset($data['merchant'])){
            if($data['merchant'] instanceof Merchant){
                $this->setMerchant($data['merchant']);
            }elseif(is_array($data['merchant'])){
                $merchant = new Merchant();
                $merchant->photoUrl = Arr::data_get($data, 'merchant.photoUrl');

                $this->setMerchant($merchant);
            }
        }
        if(isset($data['order'])){
            if($data['order'] instanceof Order){
                $this->setOrder($data['order']);
            }elseif(is_array($data['order'])){
                $order = new Order();
                $order->currency = Arr::data_get($data, 'order.currency', $order->currency);
                $order->description = Arr::data_get($data, 'order.description');
                $order->reference = Arr::data_get($data, 'order.reference');
                $order->total = floatval(Arr::data_get($data, 'order.total'));

                $this->setOrder($order);
            }
        }
        if(isset($data['user'])){
            if($data['user'] instanceof User){
                $this->setUser($data['user']);
            }elseif(is_array($data['user'])){
                $user = new User();
                $user->email = Arr::data_get($data, 'user.email');

                $this->setUser($user);
            }
        }
        if(isset($data['url'])){
            if($data['url'] instanceof Url){
                $this->setUrl($data['url']);
            }elseif(is_array($data['url'])){
                $url = new Url();
                $url->return = Arr::data_get($data, 'url.return');
                $url->notification = Arr::data_get($data, 'url.notification');

                $this->setUrl($url);
            }
        }
        $this->sandbox = filter_var(Arr::data_get($data, 'sandbox', $this->sandbox), FILTER_VALIDATE_BOOLEAN);
    }

    #region Setters
    /**
     * @param Order $order
     */
    public function setOrder($order)
    {
        if($order instanceof Order) {
            $this->order = $order;
        }
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @param Merchant $merchant
     */
    public function setMerchant($merchant)
    {
        $this->merchant = $merchant;
    }

    /**
     * @param Url $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }
    #endregion
}