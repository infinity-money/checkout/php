<?php namespace Infinity\Checkout;

/**
 * Infinity Checkout PHP Library
 * Author: Irman Ahmad
 * Date: 2018-04-18
 * Time: 3:15 PM
 *
 * @property string $email
 *
 */

class User
{
    public $email;
}