<?php namespace Infinity\Checkout;

/**
 * Infinity Checkout PHP Library
 * Author: Irman Ahmad
 * Date: 2018-04-18
 * Time: 3:15 PM
 *
 * @property string $currency
 * @property string $description
 * @property string $reference
 * @property float|int $total
 *
 */

class Order
{
    public $currency = 'MYR';
    public $description;
    public $reference;
    public $total;
}