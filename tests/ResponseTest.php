<?php

/**
 *  Corresponding Class to test YourClass class
 *
 *  For each class in your library, there should be a corresponding Unit-Test for it
 *  Unit-Tests should be as much as possible independent from other test going on.
 *
 * @author Irman Ahmad
 */
class ResponseTest extends \PHPUnit\Framework\TestCase
{

    /**
     * Just check if the YourClass has no syntax error
     *
     * This is just a simple check to make sure your library has no syntax error. This helps you troubleshoot
     * any typo before you even use this library in a real project.
     *
     */
    public function testEverything()
    {
        $secret = 'b31579613fa07a62d1b055fd668cdd3ffd1d3002';
        $data = array(
            'merchant-key' => 'mykey',
            'order-reference' => 'sample-order',
            'status' => 'captured'
        );
        $data['signature'] = \Infinity\Checkout\Response::generateSignature($data, $secret);

        $response = \Infinity\Checkout\Response::parse($data);
        $this->assertTrue($response->isSignatureValid($secret));
    }

}