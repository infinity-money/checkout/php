# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security

## [1.0.3] - 2018-08-29
### Added
- Added Tests.
- Added GitLab CI.

### Fixed
- Fixed `Response` class.

## [1.0.2] - 2018-08-27
### Added
- Added response class, to help simplify notification and return handling.

## [1.0.1] - 2018-08-27
### Added
- Added the ability to use custom JS path.
- Added loading message.

## [1.0.0] - 2018-08-21
### Added
- Added this changelog.
- Added URL return.